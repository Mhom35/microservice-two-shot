import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatsList from './HatsList';
import MainPage from './MainPage';
import HatDetail from './HatDetail';
import HatForm from './HatForm';
import HatDelete from './HatDelete';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import ShoeDetail from './ShoesDetail';

function App(props) {

  if (props.hats === undefined){
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="shoes">
            <Route index element={<ShoesList shoes={props.shoes} />}/>
            <Route path="new" element={<ShoeForm />} />
            <Route path="detail/:id" element={<ShoeDetail />}/>
        </Route>
        <Route path="hats" >
          <Route index element={<HatsList hats={props.hats}/>} />
          <Route path="new" element={<HatForm />} />
          <Route path=':hatId'>
            <Route index element={<HatDetail />} />
            <Route path='delete' element={<HatDelete />} />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
