import React, { useCallback, useState, useEffect, useSyncExternalStore} from "react";
import { Link } from "react-router-dom";

function HatsList(props) {
  const [listData, setListData] = useState([props.hats]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch('http://localhost:8090/api/hats/');
      if (response.ok) {
        const data = await response.json();
        setListData(data.hats);
      }
    }
    getData();
  }, []);

  console.log("LISTDATA ------" ,listData[0].href);

  return (
    <div className="row row-cols-1 row-cols-md-3 g-4 m-4">
      {listData.map( hat => {
        return (
          <div className="col" key={hat.href}>
            <div className="card shadow h-100" >
              <img src={hat.picture_url} className="card-img-top img-fluid" alt="oops.."/>
              <div className="card-body">
                <h5 className="card-title">{hat.style}</h5>
                <p className="card-text">A {hat.color} hat made of {hat.fabric} in {hat.style} style.</p>
              </div>
              <div className="p-3">
                <Link to={`/hats/${hat.id}`} className="btn btn-primary stretched-link">See details</Link>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default HatsList;
