import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";

function HatDetail() {
  const params = useParams();
  const [hatData, setHatData] = useState({});
  const [locationData, setlocationData] = useState({});

  useEffect(() => {
    const getData = async () => {
      const response = await fetch(`http://localhost:8090/api/hats/${params.hatId}`);
      if (response.ok) {
        const data = await response.json();
        setHatData(data);
        setlocationData(data.location);
      }
    }
    getData();
  }, []);

  return (
    <div className="my-5 container">
      <div className="d-flex justify-content-center mt-5">
        <div className="card mb-3 shadow" style={{width: "30rem"}}>
          <div className="card-header">
            <h5>{locationData.closet_name}</h5>
          </div>
          <img src={hatData.picture_url} className="card-img-top" alt="oops.."/>
          <div className="card-body">
            <h3 className="card-title">{hatData.style}</h3>
            <p className="card-text">A {hatData.color} hat made of {hatData.fabric} in {hatData.style} style.</p>
          </div>
          <div className="card-footer p-3 ">
            <Link to="/hats" className="btn btn-primary me-3">Go back</Link>
            <Link to="delete" className="btn btn-danger">Delete</Link>
          </div>
        </div>
      </div>
    </div>
);
}

export default HatDetail;
