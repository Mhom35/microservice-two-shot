import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadHatsAndShoes() {
  const response1 = await fetch('http://localhost:8090/api/hats/');
  let data1 = null;
  let data2 = null;
  if (response1.ok) {
    data1 = await response1.json();

  } else (
    console.error(response1)
  )

  const response2 = await fetch('http://localhost:8080/api/bins/shoes/')
  if (response2.ok) {
    data2 = await response2.json();
  } else (
    console.error(response2)
  )

  console.log(data2);


  root.render(
    <React.StrictMode>
      <App hats={data1.hats} shoes={data2.shoes}/>
    </React.StrictMode>
  );
}

  loadHatsAndShoes();
